/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java.View;

import com.mycompany.de12_java.Controller.CongNhanController;
import com.mycompany.de12_java.Model.CongNhan;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author long
 */
public class CongNhanView extends JPanel {

    private JLabel lbMaCongNhan, lbTenCongNhan, lbDiaChiCongNhan, lbSoDienThoaiCongNhan, lbCaSanXuat;
    private JTextField tfMaCongNhan, tfTenCongNhan, tfSoDienThoaiCongNhan;
    private JComboBox cbDiaChiCongNhan, cbCaSanXuatCongNhan;
    private JButton btnThemCongNhan, btnSuaCongNhan, btnXoaCongNhan, btnLamMoiCongNhan;
    private JTable tableCongNhan;
    private JScrollPane jScrollPaneCongNhan;
    private DefaultTableModel modelCongNhan;
    private CongNhanController congNhanController;

    public CongNhanView() {
        lbMaCongNhan = new JLabel("Mã công nhân");
        lbTenCongNhan = new JLabel("Tên công nhân");
        lbDiaChiCongNhan = new JLabel("Địa chỉ");
        lbSoDienThoaiCongNhan = new JLabel("Số điện thoại");
        lbCaSanXuat = new JLabel("Ca sản xuất");
        tfMaCongNhan = new JTextField();
        tfTenCongNhan = new JTextField();
        tfSoDienThoaiCongNhan = new JTextField();
        cbDiaChiCongNhan = new JComboBox();
        cbCaSanXuatCongNhan = new JComboBox();
        btnThemCongNhan = new JButton("Thêm");
        btnSuaCongNhan = new JButton("Sửa");
        btnXoaCongNhan = new JButton("Xóa");
        btnLamMoiCongNhan = new JButton("Làm mới");
        modelCongNhan = new DefaultTableModel(new Object[][]{},
                new String[]{"Mã công nhân", "Họ Tên", "Địa chỉ", "Số điện thoại", "Ca sản xuất"});
        tableCongNhan = new JTable(modelCongNhan);
        jScrollPaneCongNhan = new JScrollPane(tableCongNhan);
        congNhanController = new CongNhanController();

        settingsJPanel();
        settingsJLabel();
        settingsJTextField();
        settingsJCombobox();
        settingJButton();
        settingJTable();
        settingsJScrollPane();
    }

    private void settingsJPanel() {
        this.add(lbMaCongNhan);
        this.add(lbTenCongNhan);
        this.add(lbDiaChiCongNhan);
        this.add(lbSoDienThoaiCongNhan);
        this.add(lbCaSanXuat);
        this.add(tfMaCongNhan);
        this.add(tfTenCongNhan);
        this.add(tfSoDienThoaiCongNhan);
        this.add(cbDiaChiCongNhan);
        this.add(cbCaSanXuatCongNhan);
        this.add(btnThemCongNhan);
        this.add(btnSuaCongNhan);
        this.add(btnXoaCongNhan);
        this.add(btnLamMoiCongNhan);
        this.add(jScrollPaneCongNhan);
        this.setLayout(null);
    }

    private void settingsJLabel() {
        lbMaCongNhan.setBounds(70, 40, 120, 30);
        lbDiaChiCongNhan.setBounds(70, 100, 120, 30);
        lbTenCongNhan.setBounds(670, 40, 120, 30);
        lbSoDienThoaiCongNhan.setBounds(670, 100, 120, 30);
        lbCaSanXuat.setBounds(70, 160, 120, 30);
    }

    private void settingsJTextField() {
        tfMaCongNhan.setBounds(210, 40, 400, 30);
        tfTenCongNhan.setBounds(810, 40, 400, 30);
        tfSoDienThoaiCongNhan.setBounds(810, 100, 400, 30);
        tfSoDienThoaiCongNhan.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent e) {
                tf_soDienThoaiCongNhanKeyTyped(e);
            }
        });
    }

    private void settingsJCombobox() {
        cbDiaChiCongNhan.setBounds(210, 100, 400, 30);
        cbDiaChiCongNhan.addItem("Hà Nội");
        cbDiaChiCongNhan.addItem("Hưng Yên");
        cbDiaChiCongNhan.addItem("Hà Nam");
        cbDiaChiCongNhan.addItem("Bắc Giang");
        cbDiaChiCongNhan.addItem("Bắc Ninh");
        cbDiaChiCongNhan.addItem("Nam Định");
        cbDiaChiCongNhan.addItem("Hải Phòng");
        cbDiaChiCongNhan.addItem("Thanh Hóa");
        cbDiaChiCongNhan.addItem("Thái Bình");
        cbDiaChiCongNhan.addItem("Nghệ An");
        cbDiaChiCongNhan.addItem("Thái Nguyên");
        cbDiaChiCongNhan.addItem("Hà Giang");
        cbDiaChiCongNhan.setEditable(true);
        cbCaSanXuatCongNhan.setBounds(210, 160, 400, 30);
        cbCaSanXuatCongNhan.addItem("Sáng");
        cbCaSanXuatCongNhan.addItem("Chiều");
        cbCaSanXuatCongNhan.addItem("Đêm");
    }

    private void settingJButton() {
        btnThemCongNhan.setBounds(100, 240, 150, 40);
        btnThemCongNhan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnThemCongNhanActionPerformed(e);
            }
        });
        btnSuaCongNhan.setBounds(350, 240, 150, 40);
        btnSuaCongNhan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnSuaCongNhanActionPerformed(e);
            }
        });
        btnXoaCongNhan.setBounds(780, 240, 150, 40);
        btnXoaCongNhan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnXoaCongNhanActionPerformed(e);
            }
        });
        btnLamMoiCongNhan.setBounds(1030, 240, 150, 40);
        btnLamMoiCongNhan.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnLamMoiCongNhanActionPerformed(e);
            }
        });
    }

    private void settingJTable() {
        tableCongNhan.setBounds(0, 310, 1280, 360);
        hienThiCongNhan();
        tableCongNhan.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableCongNhanMouseClicked(evt);
            }
        });
    }

    private void settingsJScrollPane() {
        jScrollPaneCongNhan.setBounds(0, 310, 1280, 360);
    }

    private void tf_soDienThoaiCongNhanKeyTyped(java.awt.event.KeyEvent evt) {
        char vchar = evt.getKeyChar();
        if (!(Character.isDigit(vchar)) || (vchar == KeyEvent.VK_BACK_SPACE) || (vchar == KeyEvent.VK_DELETE)) {
            evt.consume();
        }
    }

    private void btnThemCongNhanActionPerformed(java.awt.event.ActionEvent evt) {
        if (congNhanController.kiemTraMaCongNhanTonTai(tfMaCongNhan.getText()) != -1) {
            JOptionPane.showMessageDialog(null, "Mã công nhân đã tồn tại", "Công nhân", 0);
        } else if (congNhanController.kiemTraThongTinCongNhanHopLe(tfMaCongNhan.getText(),
                tfTenCongNhan.getText(), (String) cbDiaChiCongNhan.getSelectedItem(), tfSoDienThoaiCongNhan.getText())) {

            CongNhan congNhan = new CongNhan();
            congNhan.setMaCongNhan(tfMaCongNhan.getText());
            congNhan.setHoTen(tfTenCongNhan.getText());
            congNhan.setDiaChi((String) cbDiaChiCongNhan.getSelectedItem());
            congNhan.setCaSanXuat((String) cbCaSanXuatCongNhan.getSelectedItem());
            congNhan.setSoDienThoai(tfSoDienThoaiCongNhan.getText());
            int rowAffected = congNhanController.insertCongNhanMySQL(congNhan);
            if (rowAffected > 0) {
                JOptionPane.showMessageDialog(null, "Thêm công nhân thành công", "Công nhân", 2);
            } else {
                JOptionPane.showMessageDialog(null, "Thêm công nhân thất bại", "Công nhân", 0);
            }
            btnLamMoiCongNhanActionPerformed(evt);
        }
    }

    private void tableCongNhanMouseClicked(java.awt.event.MouseEvent evt) {
        int selectedRow = tableCongNhan.getSelectedRow();
        String maCongNhan = (String) tableCongNhan.getValueAt(selectedRow, 0);
        update();
        for (int i = 0; i < tableCongNhan.getRowCount(); i++) {
            if (maCongNhan.compareTo((String) tableCongNhan.getValueAt(i, 0)) == 0) {
                tableCongNhan.setRowSelectionInterval(i, i);
                break;
            }
        }
        selectedRow = tableCongNhan.getSelectedRow();
        if (selectedRow > -1) {
            CongNhan congNhan = congNhanController.layThongTinCongNhanMySql(maCongNhan);
            tfMaCongNhan.setText(congNhan.getMaCongNhan());
            tfTenCongNhan.setText(congNhan.getHoTen());
            cbDiaChiCongNhan.setSelectedItem(congNhan.getDiaChi());
            tfSoDienThoaiCongNhan.setText(congNhan.getSoDienThoai());
            cbCaSanXuatCongNhan.setSelectedItem(congNhan.getCaSanXuat());
            tfMaCongNhan.setEnabled(false);
        } else {
            JOptionPane.showMessageDialog(null, "Công nhân không tồn tại", "Công nhân", 0);
        }
    }

    private void btnSuaCongNhanActionPerformed(java.awt.event.ActionEvent evt) {
        int row = tableCongNhan.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn dòng cần sửa", "Công nhân", 0);
        } else {
            if (congNhanController.kiemTraThongTinCongNhanHopLe(tfMaCongNhan.getText(), tfTenCongNhan.getText(),
                    (String) cbDiaChiCongNhan.getSelectedItem(), tfSoDienThoaiCongNhan.getText())) {

                CongNhan congNhan = new CongNhan();
                congNhan.setMaCongNhan(tfMaCongNhan.getText());
                congNhan.setHoTen(tfTenCongNhan.getText());
                congNhan.setDiaChi((String) cbDiaChiCongNhan.getSelectedItem());
                congNhan.setCaSanXuat((String) cbCaSanXuatCongNhan.getSelectedItem());
                congNhan.setSoDienThoai(tfSoDienThoaiCongNhan.getText());
                int rowAffected = congNhanController.updateCongNhanMySQL(congNhan);
                if (rowAffected > 0) {
                    JOptionPane.showMessageDialog(null, "Sửa thông tin công nhân thành công", "Công nhân", 2);
                } else {
                    JOptionPane.showMessageDialog(null, "Công nhân cần sửa không tồn tại", "Công nhân", 0);
                }
                btnLamMoiCongNhanActionPerformed(evt);
            }
        }
    }

    private void btnXoaCongNhanActionPerformed(java.awt.event.ActionEvent evt) {
        int row = tableCongNhan.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn dòng cần xóa", "Công nhân", 0);
        } else {
            String maCongNhan = (String) tableCongNhan.getValueAt(row, 0);
            int rowAffected = congNhanController.deleteCongNhanMySQL(maCongNhan);
            if (rowAffected > 0) {
                JOptionPane.showMessageDialog(null, "Xóa thông tin công nhân thành công", "Công nhân", 2);
            } else {
                JOptionPane.showMessageDialog(null, "Công nhân cần xóa không tồn tại", "Công nhân", 0);
            }
            btnLamMoiCongNhanActionPerformed(evt);
        }
    }

    private void btnLamMoiCongNhanActionPerformed(java.awt.event.ActionEvent evt) {
        tfMaCongNhan.setText("");
        tfTenCongNhan.setText("");
        cbDiaChiCongNhan.setSelectedIndex(0);
        cbCaSanXuatCongNhan.setSelectedIndex(0);
        tfSoDienThoaiCongNhan.setText("");
        tfMaCongNhan.setEnabled(true);
        tableCongNhan.clearSelection();
        update();
    }

    public void update() {
        xoaTableCongNhan();
        hienThiCongNhan();
    }

    private void hienThiCongNhan() {
        ArrayList<CongNhan> listCongNhan = congNhanController.readCongNhanMySql();
        for (int i = 0; i < listCongNhan.size(); i++) {
            modelCongNhan.addRow(new Object[]{listCongNhan.get(i).getMaCongNhan(),
                listCongNhan.get(i).getHoTen(), listCongNhan.get(i).getDiaChi(),
                listCongNhan.get(i).getSoDienThoai(), listCongNhan.get(i).getCaSanXuat()});
        }
    }

    private void xoaTableCongNhan() {
        for (int i = 0; i < modelCongNhan.getRowCount(); i++) {
            modelCongNhan.removeRow(i);
            i--;
        }
    }

}
