/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java.View;

import com.mycompany.de12_java.Controller.SanPhamDaLamController;
import com.mycompany.de12_java.Controller.BangCongController;
import com.mycompany.de12_java.Controller.SanPhamController;
import com.mycompany.de12_java.Controller.CongNhanController;
import com.mycompany.de12_java.Model.CongNhan;
import com.mycompany.de12_java.Model.BangCong;
import com.mycompany.de12_java.Model.SanPham;
import com.mycompany.de12_java.Model.SanPhamDaLam;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import java.sql.Date;
import java.util.ArrayList;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author long
 */
public class BangCongView extends JPanel {

    private JPanel panelDanhSachBangCong, panelTaoBangCong, panelThongTinChung, panelSanPhamDaLam;
    private JTabbedPane tabbedPaneBangCong;
    private JLabel lbMaBangCong, lbMaCongNhanBC, lbTenCongNhanBC, lbDiaChiCongNhanBC, lbSoDienThoaiCongNhanBC, lbCaCongNhanBC;
    private JLabel lbTongTienBC, lbMaSanPhamBC, lbTenSanPhamBC, lbDonGiaBC, lbSoLuongBC, lbThanhTienBC;
    private JTextField tfMaBangCong, tfTenCongNhanBC, tfDiaChiCongNhanBC, tfSoDienThoaiCongNhanBC, tfCaCongNhanBC;
    private JTextField tfTongTienBC, tfTenSanPhamBC, tfDonGiaBC, tfSoLuongBC, tfThanhTienBC;
    private JComboBox cbMaCongNhanBC, cbMaSanPhamBC;
    private JButton btnThemSanPhamDaLam, btnSuaSanPhamDaLam, btnXoaSanPhamDaLam, btnLamMoiSanPhamDaLam;
    private JButton btnThemBC, btnXoaBC, btnLamMoiBC;
    private JTable tableSanPhamDaLam, tableBangCong, tableChiTietBC;
    private JScrollPane jScrollPaneSanPhamDaLam, jScrollPaneBangCong, jScrollPaneChiTietBC;
    private DefaultTableModel modelSanPhamDaLam, modelBangCong, modelChiTietBC;
    private JLabel lbTieuDe1, lbTieuDe2;
    private BangCongController bangCongController;
    private SanPhamDaLamController sanPhamDaLamController;
    private CongNhanController congNhanController;
    private SanPhamController sanPhamController;

    public BangCongView() {
        tabbedPaneBangCong = new JTabbedPane();
        panelDanhSachBangCong = new JPanel();
        panelTaoBangCong = new JPanel();
        panelSanPhamDaLam = new JPanel();
        panelThongTinChung = new JPanel();
        lbMaBangCong = new JLabel("Mã bảng công");
        lbMaCongNhanBC = new JLabel("Mã công nhân");
        lbTenCongNhanBC = new JLabel("Tên công nhân");
        lbDiaChiCongNhanBC = new JLabel("Địa chỉ");
        lbSoDienThoaiCongNhanBC = new JLabel("Số điện thoại");
        lbCaCongNhanBC = new JLabel("Ca sản xuất");
        lbMaSanPhamBC = new JLabel("Mã sản phẩm");
        lbTenSanPhamBC = new JLabel("Tên sản phẩm");
        lbDonGiaBC = new JLabel("Đơn giá");
        lbSoLuongBC = new JLabel("Số lượng");
        lbThanhTienBC = new JLabel("Thành tiền");
        lbTongTienBC = new JLabel("Tổng tiền");
        tfMaBangCong = new JTextField();
        tfTenCongNhanBC = new JTextField();
        tfDiaChiCongNhanBC = new JTextField();
        tfSoDienThoaiCongNhanBC = new JTextField();
        tfCaCongNhanBC = new JTextField();
        tfTenSanPhamBC = new JTextField();
        tfDonGiaBC = new JTextField();
        tfSoLuongBC = new JTextField();
        tfThanhTienBC = new JTextField("0");
        tfTongTienBC = new JTextField("0");
        cbMaCongNhanBC = new JComboBox();
        cbMaSanPhamBC = new JComboBox();
        btnThemSanPhamDaLam = new JButton("Thêm sản phẩm");
        btnSuaSanPhamDaLam = new JButton("Sửa sản phẩm");
        btnXoaSanPhamDaLam = new JButton("Xóa sản phẩm");
        btnLamMoiSanPhamDaLam = new JButton("Làm mới sản phẩm đã làm");
        btnThemBC = new JButton("In bảng công");
        btnXoaBC = new JButton("Xóa bảng công");
        btnLamMoiBC = new JButton("Làm mới bảng công");
        modelSanPhamDaLam = new DefaultTableModel(new Object[][]{}, new String[]{
            "Mã sản phẩm", "Tên sản phẩm", "Đơn Giá", "Số lượng", "Thành tiền"});
        tableSanPhamDaLam = new JTable(modelSanPhamDaLam);
        jScrollPaneSanPhamDaLam = new JScrollPane(tableSanPhamDaLam);
        modelBangCong = new DefaultTableModel(new Object[][]{}, new String[]{"Mã bảng công",
            "Ngày làm việc", "Mã công nhân", "Tên công nhân", "Địa chỉ", "Số điện thoại", "Ca sản xuất", "Tổng tiền"});
        tableBangCong = new JTable(modelBangCong);
        jScrollPaneBangCong = new JScrollPane(tableBangCong);
        modelChiTietBC = new DefaultTableModel(new Object[][]{}, new String[]{"Mã bảng công",
            "Mã làm sản phẩm", "Mã sản phẩm", "Tên sản phẩm", "Đơn Giá", "Số lượng", "Thành tiền"});
        tableChiTietBC = new JTable(modelChiTietBC);
        jScrollPaneChiTietBC = new JScrollPane(tableChiTietBC);
        lbTieuDe1 = new JLabel("Danh sách bảng công");
        lbTieuDe2 = new JLabel("Chi tiết bảng công");
        bangCongController = new BangCongController();
        sanPhamDaLamController = new SanPhamDaLamController();
        congNhanController = new CongNhanController();
        sanPhamController = new SanPhamController();

        settingsJTabbedPane();
        settingsJLabel();
        settingsJTextField();
        settingsJCombobox();
        settingJButton();
        settingJTable();
        settingsJScrollPane();
        settingsJPanel();
    }

    private void settingsJTabbedPane() {
        tabbedPaneBangCong.addTab("Tạo bảng tính công", panelTaoBangCong);
        tabbedPaneBangCong.addTab("Danh sách bảng tính công", panelDanhSachBangCong);
        tabbedPaneBangCong.setBounds(0, 0, 1285, 670);
        tabbedPaneBangCong.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int indexTab = tabbedPaneBangCong.getModel().getSelectedIndex();
                if (indexTab == 1) {
                    xoaTableBangCong();
                    hienThiBangCong();
                }
            }
        });
    }

    private void settingsJLabel() {
        lbMaBangCong.setBounds(70, 20, 120, 30);
        lbMaCongNhanBC.setBounds(670, 20, 120, 30);
        lbTenCongNhanBC.setBounds(70, 60, 120, 30);
        lbDiaChiCongNhanBC.setBounds(670, 60, 120, 30);
        lbSoDienThoaiCongNhanBC.setBounds(70, 100, 120, 30);
        lbCaCongNhanBC.setBounds(670, 100, 120, 30);
        lbTongTienBC.setBounds(70, 140, 120, 30);
        lbMaSanPhamBC.setBounds(70, 20, 120, 30);
        lbTenSanPhamBC.setBounds(670, 20, 120, 30);
        lbDonGiaBC.setBounds(70, 60, 120, 30);
        lbSoLuongBC.setBounds(670, 60, 170, 30);
        lbThanhTienBC.setBounds(70, 100, 120, 30);
        lbTieuDe1.setBounds(10, 10, 200, 20);
        lbTieuDe2.setBounds(10, 330, 200, 20);
    }

    private void settingsJTextField() {
        tfMaBangCong.setBounds(210, 20, 400, 30);
        tfTenCongNhanBC.setBounds(210, 60, 400, 30);
        tfTenCongNhanBC.setEnabled(false);
        tfDiaChiCongNhanBC.setBounds(810, 60, 400, 30);
        tfDiaChiCongNhanBC.setEnabled(false);
        tfSoDienThoaiCongNhanBC.setBounds(210, 100, 400, 30);
        tfSoDienThoaiCongNhanBC.setEnabled(false);
        tfCaCongNhanBC.setBounds(810, 100, 400, 30);
        tfCaCongNhanBC.setEnabled(false);
        tfTongTienBC.setBounds(210, 140, 400, 30);
        tfTongTienBC.setEnabled(false);
        tfTenSanPhamBC.setBounds(810, 20, 400, 30);
        tfTenSanPhamBC.setEnabled(false);
        tfDonGiaBC.setBounds(210, 60, 400, 30);
        tfDonGiaBC.setEnabled(false);
        tfSoLuongBC.setBounds(810, 60, 400, 30);
        tfSoLuongBC.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent e) {
                tfSoLuongBCKeyTyped(e);
            }
        });
        tfSoLuongBC.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                tfThanhTienBC.setText(String.valueOf(sanPhamDaLamController.tinhThanhTien(tfSoLuongBC.getText(), tfDonGiaBC.getText())));
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                tfThanhTienBC.setText(String.valueOf(sanPhamDaLamController.tinhThanhTien(tfSoLuongBC.getText(), tfDonGiaBC.getText())));
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                tfThanhTienBC.setText(String.valueOf(sanPhamDaLamController.tinhThanhTien(tfSoLuongBC.getText(), tfDonGiaBC.getText())));
            }
        });
        tfThanhTienBC.setBounds(210, 100, 400, 30);
        tfThanhTienBC.setEnabled(false);
    }

    private void settingsJCombobox() {
        cbMaCongNhanBC.setBounds(810, 20, 400, 30);
        cbMaCongNhanBC.addItem("");
        cbMaCongNhanBC.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMaCongNhanBCActionPerformed(evt);
            }
        });
        cbMaCongNhanBC.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cbMaCongNhanBCMousePressed(evt);
            }
        });

        cbMaSanPhamBC.setBounds(210, 20, 400, 30);
        cbMaSanPhamBC.addItem("");
        cbMaSanPhamBC.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbMaSanPhamBCActionPerformed(evt);
            }
        });
        cbMaSanPhamBC.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cbMaSanPhamBCMousePressed(evt);
            }
        });
    }

    private void settingJButton() {
        btnThemSanPhamDaLam.setBounds(150, 140, 150, 40);
        btnThemSanPhamDaLam.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnThemSanPhamDaLamActionPerformed(e);
            }
        });
        btnSuaSanPhamDaLam.setBounds(400, 140, 150, 40);
        btnSuaSanPhamDaLam.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnSuaSanPhamDaLamActionPerformed(e);
            }
        });
        btnXoaSanPhamDaLam.setBounds(650, 140, 150, 40);
        btnXoaSanPhamDaLam.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnXoaSanPhamDaLamActionPerformed(e);
            }
        });
        btnLamMoiSanPhamDaLam.setBounds(900, 140, 200, 40);
        btnLamMoiSanPhamDaLam.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnLamMoiSanPhamDaLamActionPerformed(e);
            }
        });
        btnThemBC.setBounds(400, 580, 150, 40);
        btnThemBC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnThemBCActionPerformed(e);
            }
        });
        btnLamMoiBC.setBounds(700, 580, 200, 40);
        btnLamMoiBC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnLamMoiBCActionPerformed(e);
            }
        });
        btnXoaBC.setBounds(580, 580, 150, 40);
        btnXoaBC.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btnXoaBCActionPerformed(e);
            }
        });
    }

    private void settingJTable() {
        tableSanPhamDaLam.setBounds(5, 190, 1270, 150);
        tableSanPhamDaLam.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSanPhamDaLamMouseClicked(evt);
            }
        });
        tableChiTietBC.setBounds(0, 360, 1280, 200);
        tableBangCong.setBounds(0, 40, 1280, 280);
        tableBangCong.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableBangCongMouseClicked(evt);
            }
        });
    }

    private void settingsJScrollPane() {
        jScrollPaneSanPhamDaLam.setBounds(5, 190, 1270, 150);
        jScrollPaneChiTietBC.setBounds(0, 360, 1280, 200);
        jScrollPaneBangCong.setBounds(0, 40, 1280, 280);
    }

    private void settingsJPanel() {
        this.add(tabbedPaneBangCong);
        this.setLayout(null);
        panelTaoBangCong.add(btnThemBC);
        panelTaoBangCong.add(btnLamMoiBC);
        panelTaoBangCong.add(panelSanPhamDaLam);
        panelTaoBangCong.add(panelThongTinChung);
        panelTaoBangCong.setLayout(null);
        panelDanhSachBangCong.add(jScrollPaneBangCong);
        panelDanhSachBangCong.add(btnXoaBC);
        panelDanhSachBangCong.add(jScrollPaneChiTietBC);
        panelDanhSachBangCong.add(lbTieuDe1);
        panelDanhSachBangCong.add(lbTieuDe2);
        panelDanhSachBangCong.setLayout(null);
        panelSanPhamDaLam.setBounds(0, 10, 1280, 340);
        panelSanPhamDaLam.setBorder(BorderFactory.createTitledBorder("Thông tin sản phẩm đã làm"));
        panelSanPhamDaLam.add(lbMaSanPhamBC);
        panelSanPhamDaLam.add(lbTenSanPhamBC);
        panelSanPhamDaLam.add(lbDonGiaBC);
        panelSanPhamDaLam.add(lbSoLuongBC);
        panelSanPhamDaLam.add(lbThanhTienBC);
        panelSanPhamDaLam.add(tfTenSanPhamBC);
        panelSanPhamDaLam.add(tfDonGiaBC);
        panelSanPhamDaLam.add(tfSoLuongBC);
        panelSanPhamDaLam.add(tfThanhTienBC);
        panelSanPhamDaLam.add(cbMaSanPhamBC);
        panelSanPhamDaLam.add(jScrollPaneSanPhamDaLam);
        panelSanPhamDaLam.add(btnThemSanPhamDaLam);
        panelSanPhamDaLam.add(btnSuaSanPhamDaLam);
        panelSanPhamDaLam.add(btnXoaSanPhamDaLam);
        panelSanPhamDaLam.add(btnLamMoiSanPhamDaLam);
        panelSanPhamDaLam.setLayout(null);
        panelThongTinChung.setBorder(BorderFactory.createTitledBorder("Thông tin chung"));
        panelThongTinChung.setBounds(0, 360, 1280, 220);
        panelThongTinChung.add(lbMaBangCong);
        panelThongTinChung.add(lbMaCongNhanBC);
        panelThongTinChung.add(lbTenCongNhanBC);
        panelThongTinChung.add(lbDiaChiCongNhanBC);
        panelThongTinChung.add(lbSoDienThoaiCongNhanBC);
        panelThongTinChung.add(lbCaCongNhanBC);
        panelThongTinChung.add(lbTongTienBC);
        panelThongTinChung.add(tfMaBangCong);
        panelThongTinChung.add(tfTenCongNhanBC);
        panelThongTinChung.add(tfDiaChiCongNhanBC);
        panelThongTinChung.add(tfSoDienThoaiCongNhanBC);
        panelThongTinChung.add(tfCaCongNhanBC);
        panelThongTinChung.add(lbCaCongNhanBC);
        panelThongTinChung.add(tfTongTienBC);
        panelThongTinChung.add(cbMaCongNhanBC);
        panelThongTinChung.setLayout(null);
    }

    private void btnThemSanPhamDaLamActionPerformed(java.awt.event.ActionEvent evt) {
        if (sanPhamDaLamController.kiemTraDaCoThongTinLamSanPham(tableSanPhamDaLam, (String) cbMaSanPhamBC.getSelectedItem())) {
            JOptionPane.showMessageDialog(null, "Đã có thông tin làm sản phẩm", "Bảng tính công", 0);
        } else if (sanPhamDaLamController.kiemTraThongTinLamSanPhamHopLe((String) cbMaSanPhamBC.getSelectedItem(), tfSoLuongBC.getText())) {

            SanPham sanPham = new SanPham();
            sanPham.setMaSanPham((String) cbMaSanPhamBC.getSelectedItem());
            sanPham.setTenSanPham(tfTenSanPhamBC.getText());
            sanPham.setDonGia(Integer.parseInt(tfDonGiaBC.getText()));
            SanPhamDaLam sanPhamDaLam = new SanPhamDaLam();
            sanPhamDaLam.setSanPham(sanPham);
            sanPhamDaLam.setSoLuong(Integer.parseInt(tfSoLuongBC.getText()));
            sanPhamDaLam.setThanhTien(Integer.parseInt(tfThanhTienBC.getText()));
            modelSanPhamDaLam.addRow(new Object[]{sanPhamDaLam.getSanPham().getMaSanPham(),
                sanPhamDaLam.getSanPham().getTenSanPham(), sanPhamDaLam.getSanPham().getDonGia(),
                sanPhamDaLam.getSoLuong(), sanPhamDaLam.getThanhTien()});
            tfTongTienBC.setText(String.valueOf(bangCongController.tinhTongTien(tableSanPhamDaLam)));
            btnLamMoiSanPhamDaLamActionPerformed(evt);
            JOptionPane.showMessageDialog(null, "Thêm sản phẩm vào bảng tính công thành công", "Bảng tính công", 2);
        }
    }

    private void btnSuaSanPhamDaLamActionPerformed(java.awt.event.ActionEvent evt) {
        int row = tableSanPhamDaLam.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn dòng cần sửa", "Bảng tính công", 0);
        } else {
            SanPhamDaLam sanPhamDaLam = new SanPhamDaLam();
            sanPhamDaLam.setSoLuong(Integer.parseInt(tfSoLuongBC.getText()));
            sanPhamDaLam.setThanhTien(Integer.parseInt(tfThanhTienBC.getText()));
            tableSanPhamDaLam.setValueAt(sanPhamDaLam.getSoLuong(), row, 3);
            tableSanPhamDaLam.setValueAt(sanPhamDaLam.getThanhTien(), row, 4);
            tfTongTienBC.setText(String.valueOf(bangCongController.tinhTongTien(tableSanPhamDaLam)));
            btnLamMoiSanPhamDaLamActionPerformed(evt);
            JOptionPane.showMessageDialog(null, "Sửa thông tin sản phẩm đã làm thành công", "Bảng tính công", 2);
        }
    }

    private void btnXoaSanPhamDaLamActionPerformed(java.awt.event.ActionEvent evt) {
        int row = tableSanPhamDaLam.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn dòng cần xóa", "Sản phẩm đã làm", 0);
        } else {
            modelSanPhamDaLam.removeRow(row);
            tfTongTienBC.setText(String.valueOf(bangCongController.tinhTongTien(tableSanPhamDaLam)));
            btnLamMoiSanPhamDaLamActionPerformed(evt);
            JOptionPane.showMessageDialog(null, "Xóa sản phẩm khỏi bảng tính công thành công", "Bảng tính công", 2);
        }
    }

    private void btnLamMoiSanPhamDaLamActionPerformed(java.awt.event.ActionEvent evt) {
        cbMaSanPhamBC.setSelectedIndex(0);
        tfTenSanPhamBC.setText("");
        tfDonGiaBC.setText("");
        tfSoLuongBC.setText("");
        tfThanhTienBC.setText("");
        cbMaSanPhamBC.setEnabled(true);
        tableSanPhamDaLam.clearSelection();
    }

    private void tableSanPhamDaLamMouseClicked(java.awt.event.MouseEvent evt) {
        int row = tableSanPhamDaLam.getSelectedRow();
        SanPham sanPham = new SanPham();
        sanPham.setMaSanPham((String) tableSanPhamDaLam.getValueAt(row, 0));
        sanPham.setTenSanPham((String) tableSanPhamDaLam.getValueAt(row, 1));
        sanPham.setDonGia((int) tableSanPhamDaLam.getValueAt(row, 2));
        SanPhamDaLam sanPhamDaLam = new SanPhamDaLam();
        sanPhamDaLam.setSanPham(sanPham);
        sanPhamDaLam.setSoLuong((int) tableSanPhamDaLam.getValueAt(row, 3));
        sanPhamDaLam.setThanhTien((int) tableSanPhamDaLam.getValueAt(row, 4));
        cbMaSanPhamBC.setSelectedItem(sanPhamDaLam.getSanPham().getMaSanPham());
        tfTenSanPhamBC.setText(sanPhamDaLam.getSanPham().getTenSanPham());
        tfDonGiaBC.setText(String.valueOf(sanPhamDaLam.getSanPham().getDonGia()));
        tfSoLuongBC.setText(String.valueOf(sanPhamDaLam.getSoLuong()));
        tfThanhTienBC.setText(String.valueOf(sanPhamDaLam.getThanhTien()));
        cbMaSanPhamBC.setEnabled(false);
    }

    private void cbMaCongNhanBCActionPerformed(java.awt.event.ActionEvent evt) {
        String maCongNhan = (String) cbMaCongNhanBC.getSelectedItem();
        CongNhan congNhan = congNhanController.layThongTinCongNhanMySql(maCongNhan);
        tfTenCongNhanBC.setText(congNhan.getHoTen());
        tfDiaChiCongNhanBC.setText(congNhan.getDiaChi());
        tfSoDienThoaiCongNhanBC.setText(congNhan.getSoDienThoai());
        tfCaCongNhanBC.setText(congNhan.getCaSanXuat());
    }

    private void cbMaSanPhamBCActionPerformed(java.awt.event.ActionEvent evt) {
        String maSanPham = (String) cbMaSanPhamBC.getSelectedItem();
        SanPham sanPham = sanPhamController.layThongTinSanPham(maSanPham);
        tfTenSanPhamBC.setText(sanPham.getTenSanPham());
        tfDonGiaBC.setText(String.valueOf(sanPham.getDonGia()));
        tfSoLuongBC.setText("");
    }

    private void tfSoLuongBCKeyTyped(java.awt.event.KeyEvent e) {
        char vchar = e.getKeyChar();
        if (!(Character.isDigit(vchar)) || (vchar == KeyEvent.VK_BACK_SPACE) || (vchar == KeyEvent.VK_DELETE)) {
            e.consume();
        }
    }

    private void btnThemBCActionPerformed(java.awt.event.ActionEvent evt) {
        if (!sanPhamDaLamController.kiemTraDanhSachSanPhamDaLamTonTai(modelSanPhamDaLam)) {
            tfTongTienBC.setText(String.valueOf(bangCongController.tinhTongTien(tableSanPhamDaLam)));
        } else if (bangCongController.kiemTraThongTinChungBangCongHopLe(tfMaBangCong.getText(),
                (String) cbMaCongNhanBC.getSelectedItem(), tableSanPhamDaLam)) {
            Date ngayLam = java.sql.Date.valueOf(java.time.LocalDate.now());
            CongNhan congNhan = new CongNhan();
            congNhan.setMaCongNhan((String) cbMaCongNhanBC.getSelectedItem());
            congNhan.setHoTen(tfTenCongNhanBC.getText());
            congNhan.setDiaChi(tfDiaChiCongNhanBC.getText());
            congNhan.setSoDienThoai(tfSoDienThoaiCongNhanBC.getText());
            congNhan.setCaSanXuat(tfCaCongNhanBC.getText());
            BangCong bangCong = new BangCong();
            bangCong.setMaBangCong(tfMaBangCong.getText());
            bangCong.setNgayCong(ngayLam);
            bangCong.setCongNhan(congNhan);
            bangCong.setTongTien(Integer.parseInt(tfTongTienBC.getText()));
            int rowAffected = bangCongController.insertBangCongMySQL(bangCong);
            if (rowAffected > 0) {
                sanPhamDaLamController.insertSanPhamDaLamMySQL(bangCong.getMaBangCong(), tableSanPhamDaLam);
                JOptionPane.showMessageDialog(null, "Thêm bảng tính công thành công", "Bảng tính công", 2);
            }
            btnLamMoiBCActionPerformed(evt);
        }
    }

    private void btnXoaBCActionPerformed(java.awt.event.ActionEvent evt) {
        int row = tableBangCong.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn dòng cần xóa", "Bảng tính công", 0);
        } else {
            String maBangCong = (String) tableBangCong.getValueAt(row, 0);
            sanPhamDaLamController.deleteSanPhamDaLamMySQL(maBangCong);
            bangCongController.deleteBangCongMySQL(maBangCong);
            JOptionPane.showMessageDialog(null, "Xóa bảng tính công thành công", "Bảng tính công", 2);
            xoaTableChiTietBC();
            xoaTableBangCong();
            hienThiBangCong();
        }
    }

    private void btnLamMoiBCActionPerformed(java.awt.event.ActionEvent evt) {
        tfMaBangCong.setText("");
        cbMaCongNhanBC.setSelectedIndex(0);
        tfTenCongNhanBC.setText("");
        tfDiaChiCongNhanBC.setText("");
        tfCaCongNhanBC.setText("");
        tfTongTienBC.setText("");
        btnLamMoiSanPhamDaLamActionPerformed(evt);
        xoaTableSanPhamDaLam();
    }

    private void tableBangCongMouseClicked(java.awt.event.MouseEvent evt) {
        int selectedRow = tableBangCong.getSelectedRow();
        String maBangCong = (String) tableBangCong.getValueAt(selectedRow, 0);
        xoaTableBangCong();
        hienThiBangCong();
        for (int i = 0; i < tableBangCong.getRowCount(); i++) {
            if (maBangCong.compareTo((String) tableBangCong.getValueAt(i, 0)) == 0) {
                tableBangCong.setRowSelectionInterval(i, i);
                break;
            }
        }
        selectedRow = tableBangCong.getSelectedRow();
        if (selectedRow > -1) {
            xoaTableChiTietBC();
            hienThiChiTietBC(maBangCong);
        } else {
            JOptionPane.showMessageDialog(null, "Bảng tính công không tồn tại", "Bảng tính công", 0);
        }
    }

    public void update() {
        xoaComboboxMaCongNhan();
        setComboboxMaCongNhan();
        xoaComboboxMaSanPham();
        setComboboxMaSanPham();
    }

    private void hienThiBangCong() {
        ArrayList<BangCong> listBangCong = bangCongController.readBangCong();
        for (int i = 0; i < listBangCong.size(); i++) {
            modelBangCong.addRow(new Object[]{listBangCong.get(i).getMaBangCong(),
                listBangCong.get(i).getNgayCong(), listBangCong.get(i).getCongNhan().getMaCongNhan(),
                listBangCong.get(i).getCongNhan().getHoTen(),
                listBangCong.get(i).getCongNhan().getDiaChi(),
                listBangCong.get(i).getCongNhan().getSoDienThoai(),
                listBangCong.get(i).getCongNhan().getCaSanXuat(), listBangCong.get(i).getTongTien()});
        }
    }

    private void hienThiChiTietBC(String maBangCong) {
        ArrayList<SanPhamDaLam> listChiTietBC = sanPhamDaLamController.readChiTietBC(maBangCong);
        for (int i = 0; i < listChiTietBC.size(); i++) {
            modelChiTietBC.addRow(new Object[]{listChiTietBC.get(i).getMaBangCong(),
                listChiTietBC.get(i).getMaLamSanPham(), listChiTietBC.get(i).getSanPham().getMaSanPham(),
                listChiTietBC.get(i).getSanPham().getTenSanPham(),
                listChiTietBC.get(i).getSanPham().getDonGia(),
                listChiTietBC.get(i).getSoLuong(), listChiTietBC.get(i).getThanhTien()});
        }
    }

    private void setComboboxMaCongNhan() {
        ArrayList<String> listMaCongNhan = congNhanController.readListMaCongNhan();
        for (int i = 0; i < listMaCongNhan.size(); i++) {
            cbMaCongNhanBC.addItem(listMaCongNhan.get(i));
        }
    }

    private void setComboboxMaSanPham() {
        ArrayList<String> listMaSanPham = sanPhamController.readListMaSanPham();
        for (int i = 0; i < listMaSanPham.size(); i++) {
            cbMaSanPhamBC.addItem(listMaSanPham.get(i));
        }
    }

    private void xoaComboboxMaCongNhan() {
        for (int i = cbMaCongNhanBC.getItemCount() - 1; i > 0; i--) {
            cbMaCongNhanBC.removeItemAt(i);
        }
    }

    private void xoaComboboxMaSanPham() {
        for (int i = cbMaSanPhamBC.getItemCount() - 1; i > 0; i--) {
            cbMaSanPhamBC.removeItemAt(i);
        }
    }

    private void cbMaCongNhanBCMousePressed(java.awt.event.MouseEvent evt) {
        xoaComboboxMaCongNhan();
        setComboboxMaCongNhan();
    }

    private void cbMaSanPhamBCMousePressed(java.awt.event.MouseEvent evt) {
        xoaComboboxMaSanPham();
        setComboboxMaSanPham();
    }

    private void xoaTableBangCong() {
        for (int i = 0; i < modelBangCong.getRowCount(); i++) {
            modelBangCong.removeRow(i);
            i--;
        }
    }

    private void xoaTableChiTietBC() {
        for (int i = 0; i < modelChiTietBC.getRowCount(); i++) {
            modelChiTietBC.removeRow(i);
            i--;
        }
    }

    private void xoaTableSanPhamDaLam() {
        for (int i = 0; i < modelSanPhamDaLam.getRowCount(); i++) {
            modelSanPhamDaLam.removeRow(i);
            i--;
        }
    }

}
