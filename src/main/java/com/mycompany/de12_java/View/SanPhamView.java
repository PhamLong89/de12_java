/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java.View;

import com.mycompany.de12_java.Controller.SanPhamController;
import com.mycompany.de12_java.Model.SanPham;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author long
 */
public class SanPhamView extends JPanel {

    private JLabel lbMaSanPham, lbTenSanPham, lbDonGia;
    private JTextField tfMaSanPham, tfTenSanPham, tfDonGia;
    private JButton btnThemSanPham, btnSuaSanPham, btnXoaSanPham, btnLamMoiSanPham;
    private JTable tableSanPham;
    private JScrollPane jScrollPaneSanPham;
    private DefaultTableModel modelSanPham;
    private SanPhamController sanPhamController;

    public SanPhamView() {
        lbMaSanPham = new JLabel("Mã sản phẩm");
        lbTenSanPham = new JLabel("Tên sản phẩm");
        lbDonGia = new JLabel("Đơn giá");
        tfMaSanPham = new JTextField();
        tfTenSanPham = new JTextField();
        tfDonGia = new JTextField();
        btnThemSanPham = new JButton("Thêm");
        btnSuaSanPham = new JButton("Sửa");
        btnXoaSanPham = new JButton("Xóa");
        btnLamMoiSanPham = new JButton("Làm mới");
        modelSanPham = new DefaultTableModel(new Object[][]{}, new String[]{"Mã sản phẩm", "Tên sản phẩm", "Đơn giá"});
        tableSanPham = new JTable(modelSanPham);
        jScrollPaneSanPham = new JScrollPane(tableSanPham);
        sanPhamController = new SanPhamController();

        settingsJPanel();
        settingsJLabel();
        settingsJTextField();
        settingJButton();
        settingJTable();
        settingsJScrollPane();
    }

    private void settingsJPanel() {
        this.add(lbMaSanPham);
        this.add(lbTenSanPham);
        this.add(lbDonGia);
        this.add(tfMaSanPham);
        this.add(tfTenSanPham);
        this.add(tfDonGia);
        this.add(btnThemSanPham);
        this.add(btnSuaSanPham);
        this.add(btnXoaSanPham);
        this.add(btnLamMoiSanPham);
        this.add(jScrollPaneSanPham);
        this.setLayout(null);
    }

    private void settingsJLabel() {
        lbMaSanPham.setBounds(70, 40, 120, 30);
        lbDonGia.setBounds(70, 100, 120, 30);
        lbTenSanPham.setBounds(670, 40, 120, 30);
    }

    private void settingsJTextField() {
        tfMaSanPham.setBounds(210, 40, 400, 30);
        tfDonGia.setBounds(210, 100, 400, 30);
        tfTenSanPham.setBounds(810, 40, 400, 30);
        tfDonGia.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent e) {
                tfDonGiaKeyTyped(e);
            }
        });
    }

    private void settingJButton() {
        btnThemSanPham.setBounds(100, 200, 150, 40);
        btnThemSanPham.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                btnThemSanPhamActionPerformed(e);
            }
        });
        btnSuaSanPham.setBounds(350, 200, 150, 40);
        btnSuaSanPham.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                btnSuaSanPhamActionPerformed(e);
            }
        });
        btnXoaSanPham.setBounds(780, 200, 150, 40);
        btnXoaSanPham.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                btnXoaSanPhamActionPerformed(e);
            }
        });
        btnLamMoiSanPham.setBounds(1030, 200, 150, 40);
        btnLamMoiSanPham.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent e) {
                btnLamMoiSanPhamActionPerformed(e);
            }
        });
    }

    private void settingJTable() {
        tableSanPham.setBounds(0, 290, 1280, 380);
        tableSanPham.addMouseListener(new java.awt.event.MouseAdapter() {
            @Override
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableSanPhamMouseClicked(evt);
            }
        });
    }

    private void settingsJScrollPane() {
        jScrollPaneSanPham.setBounds(0, 290, 1280, 380);
    }

    private void tfDonGiaKeyTyped(java.awt.event.KeyEvent e) {
        char vchar = e.getKeyChar();
        if (!(Character.isDigit(vchar)) || (vchar == KeyEvent.VK_BACK_SPACE) || (vchar == KeyEvent.VK_DELETE)) {
            e.consume();
        }
    }

    private void btnThemSanPhamActionPerformed(java.awt.event.ActionEvent e) {
        if (sanPhamController.kiemTraMaSanPhamTonTai(tfMaSanPham.getText()) != -1) {
            JOptionPane.showMessageDialog(null, "Mã sản phẩm đã tồn tại", "Sản phẩm", 0);
        } else if (sanPhamController.kiemTraThongTinMatHangHopLe(
                tfMaSanPham.getText(), tfTenSanPham.getText(), tfDonGia.getText())) {
            SanPham sanPham = new SanPham();
            sanPham.setMaSanPham(tfMaSanPham.getText());
            sanPham.setTenSanPham(tfTenSanPham.getText());
            sanPham.setDonGia(Integer.parseInt(tfDonGia.getText()));
            int rowAffected = sanPhamController.insertSanPhamMySQL(sanPham);
            if (rowAffected > 0) {
                JOptionPane.showMessageDialog(null, "Thêm sản phẩm thành công", "Sản phẩm", 2);
            } else {
                JOptionPane.showMessageDialog(null, "Thêm sản phẩm thất bại", "Sản phẩm", 0);
            }
            btnLamMoiSanPhamActionPerformed(e);
        }
    }

    private void tableSanPhamMouseClicked(java.awt.event.MouseEvent evt) {
        int selectedRow = tableSanPham.getSelectedRow();
        String maSanPham = (String) tableSanPham.getValueAt(selectedRow, 0);
        update();
        for (int i = 0; i < tableSanPham.getRowCount(); i++) {
            if (maSanPham.compareTo((String) tableSanPham.getValueAt(i, 0)) == 0) {
                tableSanPham.setRowSelectionInterval(i, i);
                break;
            }
        }
        selectedRow = tableSanPham.getSelectedRow();
        if (selectedRow > -1) {
            SanPham sanPham = sanPhamController.layThongTinSanPham(maSanPham);
            tfMaSanPham.setText(sanPham.getMaSanPham());
            tfTenSanPham.setText(sanPham.getTenSanPham());
            tfDonGia.setText(String.valueOf(sanPham.getDonGia()));
            tfMaSanPham.setEnabled(false);
        } else {
            JOptionPane.showMessageDialog(null, "Sản phẩm không tồn tại", "Sản phẩm", 0);
        }
    }

    private void btnSuaSanPhamActionPerformed(java.awt.event.ActionEvent evt) {
        int row = tableSanPham.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn dòng cần sửa", "Sản phẩm", 0);
        } else {
            if (sanPhamController.kiemTraThongTinMatHangHopLe(tfMaSanPham.getText(),
                    tfTenSanPham.getText(), tfDonGia.getText())) {
                SanPham sanPham = new SanPham();
                sanPham.setMaSanPham(tfMaSanPham.getText());
                sanPham.setTenSanPham(tfTenSanPham.getText());
                sanPham.setDonGia(Integer.parseInt(tfDonGia.getText()));
                int rowAffected = sanPhamController.updateSanPhamMySQL(sanPham);
                if (rowAffected > 0) {
                    JOptionPane.showMessageDialog(null, "Sửa thông tin sản phẩm thành công", "Sản phẩm", 2);
                } else {
                    JOptionPane.showMessageDialog(null, "Sản phẩm cần sửa không tồn tại", "Sản phẩm", 0);
                }
                btnLamMoiSanPhamActionPerformed(evt);
            }
        }
    }

    private void btnXoaSanPhamActionPerformed(java.awt.event.ActionEvent evt) {
        int row = tableSanPham.getSelectedRow();
        if (row < 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn dòng cần xóa", "Sản phẩm", 0);
        } else {
            String maSanPham = (String) tableSanPham.getValueAt(row, 0);
            int rowAffected = sanPhamController.deleteSanPhamMySQL(maSanPham);
            if (rowAffected > 0) {
                JOptionPane.showMessageDialog(null, "Xóa thông tin sản phẩm thành công", "Sản phẩm", 2);
            } else {
                JOptionPane.showMessageDialog(null, "Sản phẩm cần xóa không tồn tại", "Sản phẩm", 0);
            }
            btnLamMoiSanPhamActionPerformed(evt);
        }
    }

    private void btnLamMoiSanPhamActionPerformed(java.awt.event.ActionEvent evt) {
        tfMaSanPham.setText("");
        tfTenSanPham.setText("");
        tfDonGia.setText("");
        tfMaSanPham.setEnabled(true);
        tableSanPham.clearSelection();
        update();
    }

    public void update() {
        xoaTableSanPham();
        hienThiSanPham();
    }

    private void hienThiSanPham() {
        ArrayList<SanPham> listSanPham = sanPhamController.readSanPham();
        for (int i = 0; i < listSanPham.size(); i++) {
            modelSanPham.addRow(new Object[]{listSanPham.get(i).getMaSanPham(),
                listSanPham.get(i).getTenSanPham(), listSanPham.get(i).getDonGia()});
        }
    }

    private void xoaTableSanPham() {
        for (int i = 0; i < modelSanPham.getRowCount(); i++) {
            modelSanPham.removeRow(i);
            i--;
        }
    }

}
