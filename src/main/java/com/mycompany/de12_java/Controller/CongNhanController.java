/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java.Controller;

import com.mycompany.de12_java.Model.CongNhan;
import com.mycompany.de12_java.Model.MySQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author long
 */
public class CongNhanController {

    private MySQL mysql;

    public CongNhanController() {
        mysql = new MySQL();
    }

    public boolean kiemTraSoDienThoai(String soDienThoai) {
        if (soDienThoai.length() == 0) {
            return true;
        } else if (soDienThoai.length() < 10 || soDienThoai.length() > 11 || soDienThoai.charAt(0) != '0') {
            return false;
        }
        return true;
    }

    public int kiemTraMaCongNhanTonTai(String maCongNhan) {
        Connection conn = mysql.getConnect();
        Statement stmt = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery("SELECT MaCongNhan FROM CongNhan");
                while (rs.next()) {
                    if (maCongNhan.compareTo(rs.getString("MaCongNhan")) == 0) {
                        return (rs.getRow() - 1);
                    }
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Kiểm tra mã công nhân tồn tại thất bại", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return -1;
    }

    public boolean kiemTraThongTinCongNhanHopLe(String maCongNhan, String hoTen, String diaChi, String soDienThoai) {
        if (maCongNhan.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa nhập mã công nhân", "Công nhân", 0);
            return false;
        } else if (maCongNhan.length() > 10) {
            JOptionPane.showMessageDialog(null, "Mã công nhân dài quá giới hạn(10)", "Công nhân", 0);
            return false;
        }
        if (hoTen.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa nhập họ tên công nhân", "Công nhân", 0);
            return false;
        } else if (hoTen.length() > 50) {
            JOptionPane.showMessageDialog(null, "Họ tên công nhân dài quá giới hạn(50)", "Công nhân", 0);
            return false;
        }
        if (diaChi.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn địa chỉ công nhân", "Công nhân", 0);
            return false;
        }
        if (!kiemTraSoDienThoai(soDienThoai)) {
            JOptionPane.showMessageDialog(null, "Số điện thoại không hợp lệ", "Công nhân", 0);
            return false;
        }
        return true;
    }

    public int insertCongNhanMySQL(CongNhan congNhan) {
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        if (conn != null) {
            try {
                ps = conn.prepareStatement("insert into CongNhan(MaCongNhan, HoTen, DiaChi, CaSanXuat, SoDienThoai) values(?,?,?,?,?)");
                ps.setString(1, congNhan.getMaCongNhan());
                ps.setString(2, congNhan.getHoTen());
                ps.setString(3, congNhan.getDiaChi());
                ps.setString(4, congNhan.getCaSanXuat());
                ps.setString(5, congNhan.getSoDienThoai());
                return ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return 0;
    }

    public int updateCongNhanMySQL(CongNhan congNhan) {
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        if (conn != null) {
            try {
                String sql = "UPDATE CongNhan SET HoTen = ?, DiaChi = ?, CaSanXuat = ?, SoDienThoai = ? WHERE MaCongNhan = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, congNhan.getHoTen());
                ps.setString(2, congNhan.getDiaChi());
                ps.setString(3, congNhan.getCaSanXuat());
                ps.setString(4, congNhan.getSoDienThoai());
                ps.setString(5, congNhan.getMaCongNhan());
                return ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return 0;
    }

    public int deleteCongNhanMySQL(String maCongNhan) {
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        if (conn != null) {
            try {
                String sql = "DELETE FROM CongNhan WHERE MaCongNhan = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, maCongNhan);
                return ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return 0;
    }

    public ArrayList<CongNhan> readCongNhanMySql() {
        ArrayList<CongNhan> listCongNhan = new ArrayList<>();
        Connection conn = mysql.getConnect();
        Statement stmt = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery("SELECT * FROM CongNhan");
                while (rs.next()) {
                    CongNhan congNhan = new CongNhan();
                    congNhan.setMaCongNhan(rs.getString("MaCongNhan"));
                    congNhan.setHoTen(rs.getString("HoTen"));
                    congNhan.setDiaChi(rs.getString("DiaChi"));
                    congNhan.setSoDienThoai(rs.getString("SoDienThoai"));
                    congNhan.setCaSanXuat(rs.getString("CaSanXuat"));
                    listCongNhan.add(congNhan);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return listCongNhan;
    }

    public ArrayList<String> readListMaCongNhan() {
        ArrayList<String> listMaCongNhan = new ArrayList<>();
        Connection conn = mysql.getConnect();
        Statement stmt = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery("SELECT MaCongNhan FROM CongNhan");
                while (rs.next()) {
                    listMaCongNhan.add(rs.getString("MaCongNhan"));
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return listMaCongNhan;
    }

    public CongNhan layThongTinCongNhanMySql(String maCongNhan) {
        CongNhan congNhan = new CongNhan();
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                String sql = "SELECT * FROM CongNhan WHERE MaCongNhan = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, maCongNhan);
                rs = ps.executeQuery();
                while (rs.next()) {
                    congNhan.setMaCongNhan(rs.getString("MaCongNhan"));
                    congNhan.setHoTen(rs.getString("HoTen"));
                    congNhan.setDiaChi(rs.getString("DiaChi"));
                    congNhan.setSoDienThoai(rs.getString("SoDienThoai"));
                    congNhan.setCaSanXuat(rs.getString("CaSanXuat"));
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return congNhan;
    }

}
