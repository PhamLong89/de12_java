/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java.Controller;

import com.mycompany.de12_java.Model.BangCong;
import com.mycompany.de12_java.Model.CongNhan;
import com.mycompany.de12_java.Model.MySQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author long
 */
public class BangCongController {

    private CongNhanController congNhanController;
    private MySQL mysql;

    public BangCongController() {
        congNhanController = new CongNhanController();
        mysql = new MySQL();
    }

    public int kiemTraMaBangCongTonTai(String maBangCong) {
        Connection conn = mysql.getConnect();
        Statement stmt = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery("SELECT MaBangCong FROM BangCong");
                while (rs.next()) {
                    if (maBangCong.compareTo(rs.getString("MaBangCong")) == 0) {
                        return (rs.getRow() - 1);
                    }
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Kiểm tra mã bảng tính công tồn tại thất bại", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return -1;
    }

    public boolean kiemTraThongTinChungBangCongHopLe(String maBangCong, String maCongNhan, JTable tableSanPhamDaLam) {
        if (maBangCong.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa nhập mã bảng công", "Bảng tính công", 0);
            return false;
        } else if (maBangCong.length() > 10) {
            JOptionPane.showMessageDialog(null, "Mã bảng tính công dài quá giới hạn(10)", "Bảng tính công", 0);
            return false;
        } else if (kiemTraMaBangCongTonTai(maBangCong) != -1) {
            JOptionPane.showMessageDialog(null, "Mã bảng tính công đã tồn tại", "Bảng tính công", 0);
            return false;
        }
        if (maCongNhan.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn mã công nhân", "Bảng tính công", 0);
            return false;
        } else if (congNhanController.kiemTraMaCongNhanTonTai(maCongNhan) == -1) {
            JOptionPane.showMessageDialog(null, "Mã công nhân không tồn tại", "Bảng tính công", 0);
            return false;
        }
        if (tableSanPhamDaLam.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "Danh sách sản phẩm  đã làm trống", "Bảng tính công", 0);
            return false;
        }
        return true;
    }

    public int insertBangCongMySQL(BangCong bangCong) {
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        if (conn != null) {
            try {
                ps = conn.prepareStatement("INSERT INTO BangCong(MaBangCong, MaCongNhan, NgayCong) values(?,?,?)");
                ps.setString(1, bangCong.getMaBangCong());
                ps.setString(2, bangCong.getCongNhan().getMaCongNhan());
                ps.setDate(3, bangCong.getNgayCong());
                return ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Thêm bảng tính công vào cơ sở dữ liệu thất bại", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return 0;
    }

    public void deleteBangCongMySQL(String maBangCong) {
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        if (conn != null) {
            try {
                String sql = "DELETE FROM BangCong WHERE MaBangCong = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, maBangCong);
                ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Xóa thông tin bảng công trong cơ sở dữ liệu thất bại", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    public ArrayList<BangCong> readBangCong() {
        ArrayList<BangCong> listBangCong = new ArrayList<>();
        Connection conn = mysql.getConnect();
        Statement stmt = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery("SELECT bc.MaBangCong, bc.NgayCong, bc.MaCongNhan, cn.HoTen, cn.DiaChi, cn.CaSanXuat, cn.SoDienThoai, tinh_tong_tien(MaBangCong) "
                        + "FROM BangCong bc INNER JOIN CongNhan cn ON bc.MaCongNhan = cn.MaCongNhan");
                while (rs.next()) {
                    CongNhan congNhan = new CongNhan();
                    congNhan.setMaCongNhan(rs.getString("MaCongNhan"));
                    congNhan.setHoTen(rs.getString("HoTen"));
                    congNhan.setDiaChi(rs.getString("DiaChi"));
                    congNhan.setSoDienThoai(rs.getString("SoDienThoai"));
                    congNhan.setCaSanXuat(rs.getString("CaSanXuat"));
                    BangCong bangCong = new BangCong();
                    bangCong.setMaBangCong(rs.getString("MaBangCong"));
                    bangCong.setNgayCong(rs.getDate("NgayCong"));
                    bangCong.setCongNhan(congNhan);
                    bangCong.setTongTien(rs.getInt("tinh_tong_tien(MaBangCong)"));
                    listBangCong.add(bangCong);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Hiển thị danh sách bảng tính công thất bại", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return listBangCong;
    }

    public int tinhTongTien(JTable table_chiTietHD) {
        int tongTien = 0;
        for (int i = 0; i < table_chiTietHD.getRowCount(); i++) {
            tongTien = tongTien + (int) table_chiTietHD.getValueAt(i, 4);
        }
        return tongTien;
    }
}
