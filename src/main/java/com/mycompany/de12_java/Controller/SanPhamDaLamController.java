/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java.Controller;

import com.mycompany.de12_java.Model.MySQL;
import com.mycompany.de12_java.Model.SanPham;
import com.mycompany.de12_java.Model.SanPhamDaLam;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author long
 */
public class SanPhamDaLamController {

    private SanPhamController sanPhamController;
    private MySQL mysql;

    public SanPhamDaLamController() {
        sanPhamController = new SanPhamController();
        mysql = new MySQL();
    }

    public boolean kiemTraDaCoThongTinLamSanPham(JTable table_sanPhamDaLam, String maSanPham) {
        for (int i = 0; i < table_sanPhamDaLam.getRowCount(); i++) {
            if (maSanPham.compareTo((String) table_sanPhamDaLam.getValueAt(i, 0)) == 0) {
                return true;
            }
        }
        return false;
    }

    public boolean kiemTraThongTinLamSanPhamHopLe(String maSanPham, String soLuong) {
        if (maSanPham.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa chọn mã sản phẩm", "Bảng tính công", 0);
            return false;
        } else if (sanPhamController.kiemTraMaSanPhamTonTai(maSanPham) == -1) {
            JOptionPane.showMessageDialog(null, "Sản phẩm không tồn tại", "Bảng tính công", 0);
            return false;
        }
        if (soLuong.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa nhập số lượng sản phẩm đã làm", "Bảng tính công", 0);
            return false;
        } else if (Integer.parseInt(soLuong) < 1) {
            JOptionPane.showMessageDialog(null, "Số lượng sản phẩm đã làm không hợp lệ(>0)", "Bảng tính công", 0);
            return false;
        }
        return true;
    }

    public boolean kiemTraDanhSachSanPhamDaLamTonTai(DefaultTableModel modelSanPhamDaLam) {
        boolean hopLe = true;
        for (int i = 0; i < modelSanPhamDaLam.getRowCount(); i++) {
            String maSanPham = (String) modelSanPhamDaLam.getValueAt(i, 0);
            if (sanPhamController.kiemTraMaSanPhamTonTai(maSanPham) == -1) {
                JOptionPane.showMessageDialog(null, "Sản phẩm có mã " + maSanPham + " không tồn tại", "Bảng tính công", 0);
                modelSanPhamDaLam.removeRow(i);
                hopLe = false;
            }
        }
        return hopLe;
    }

    public int tinhThanhTien(String soLuong, String gia) {
        try {
            int soLuong1 = Integer.parseInt(soLuong);
            int gia1 = Integer.parseInt(gia);
            return soLuong1 * gia1;
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public void insertSanPhamDaLamMySQL(String maBangCong, JTable table_sanPhamDaLam) {
        for (int i = 0; i < table_sanPhamDaLam.getRowCount(); i++) {
            SanPham sanPham = new SanPham();
            sanPham.setMaSanPham((String) table_sanPhamDaLam.getValueAt(i, 0));
            sanPham.setDonGia(Integer.parseInt(String.valueOf(table_sanPhamDaLam.getValueAt(i, 2))));
            SanPhamDaLam sanPhamDaLam = new SanPhamDaLam();
            sanPhamDaLam.setMaBangCong(maBangCong);
            sanPhamDaLam.setSanPham(sanPham);
            sanPhamDaLam.setSoLuong(Integer.parseInt(String.valueOf(table_sanPhamDaLam.getValueAt(i, 3))));
            Connection conn = mysql.getConnect();
            PreparedStatement ps = null;
            try {
                ps = conn.prepareStatement("INSERT INTO SanPhamDaLam(MaBangCong, MaSanPham, DonGia, SoLuong) values(?,?,?,?)");
                ps.setString(1, sanPhamDaLam.getMaBangCong());
                ps.setString(2, sanPhamDaLam.getSanPham().getMaSanPham());
                ps.setInt(3, sanPhamDaLam.getSanPham().getDonGia());
                ps.setInt(4, sanPhamDaLam.getSoLuong());
                ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Thêm sản phẩm đã làm vào cơ sở dữ liệu thất bại", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    public void deleteSanPhamDaLamMySQL(String maBangCong) {
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        if (conn != null) {
            try {
                String sql = "DELETE FROM SanPhamDaLam WHERE MaBangCong = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, maBangCong);
                ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Xóa thông tin sản phẩm đã làm trong cơ sở dữ liệu thất bại", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
    }

    public ArrayList<SanPhamDaLam> readChiTietBC(String maBangCong) {
        ArrayList<SanPhamDaLam> listChiTietBC = new ArrayList<>();
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                String sql = "SELECT spdl.MaBangCong, spdl.MaLamSanPham, spdl.MaSanPham, sp.TenSanPham, spdl.DonGia, spdl.SoLuong, spdl.DonGia*spdl.SoLuong "
                        + "FROM SanPhamDaLam spdl INNER JOIN SanPham sp ON spdl.MaSanPham = sp.MaSanPham "
                        + "WHERE MaBangCong = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, maBangCong);
                rs = ps.executeQuery();
                while (rs.next()) {
                    SanPham sanPham = new SanPham();
                    sanPham.setMaSanPham(rs.getString("MaSanPham"));
                    sanPham.setTenSanPham(rs.getString("TenSanPham"));
                    sanPham.setDonGia(rs.getInt("DonGia"));
                    SanPhamDaLam sanPhamDaLam = new SanPhamDaLam();
                    sanPhamDaLam.setMaBangCong(rs.getString("MaBangCong"));
                    sanPhamDaLam.setMaLamSanPham(rs.getInt("MaLamSanPham"));
                    sanPhamDaLam.setSanPham(sanPham);
                    sanPhamDaLam.setSoLuong(rs.getInt("SoLuong"));
                    sanPhamDaLam.setThanhTien(rs.getInt("spdl.DonGia*spdl.SoLuong"));
                    listChiTietBC.add(sanPhamDaLam);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Hiển thị danh sách chi tiết bảng tính công thất bại", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return listChiTietBC;
    }
}
