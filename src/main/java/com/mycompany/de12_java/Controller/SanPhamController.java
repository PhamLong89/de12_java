/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java.Controller;

import com.mycompany.de12_java.Model.SanPham;
import com.mycompany.de12_java.Model.MySQL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/**
 *
 * @author long
 */
public class SanPhamController {

    private MySQL mysql;

    public SanPhamController() {
        mysql = new MySQL();
    }

    public int kiemTraMaSanPhamTonTai(String maSanPham) {
        Connection conn = mysql.getConnect();
        Statement stmt = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery("SELECT MaSanPham FROM SanPham");
                while (rs.next()) {
                    if (maSanPham.compareTo(rs.getString("MaSanPham")) == 0) {
                        return (rs.getRow() - 1);
                    }
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Kiểm tra mã sản phẩm tồn tại thất bại", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return -1;
    }

    public boolean kiemTraThongTinMatHangHopLe(String maSanPham, String tenSanPham, String gia) {
        if (maSanPham.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa nhập mã sản phẩm", "Sản phẩm", 0);
            return false;
        } else if (maSanPham.length() > 10) {
            JOptionPane.showMessageDialog(null, "Mã sản phẩm dài quá giới hạn(10)", "Sản phẩm", 0);
            return false;
        }
        if (tenSanPham.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa nhập tên sản phẩm", "Sản phẩm", 0);
            return false;
        } else if (tenSanPham.length() > 50) {
            JOptionPane.showMessageDialog(null, "Tên sản phẩm dài quá giới hạn", "Sản phẩm", 0);
            return false;
        }
        if (gia.trim().length() == 0) {
            JOptionPane.showMessageDialog(null, "Chưa nhập đơn giá", "Sản phẩm", 0);
            return false;
        } else if (Integer.parseInt(gia) < 1000) {
            JOptionPane.showMessageDialog(null, "Đơn giá không hợp lệ(>999đ)", "Sản phẩm", 0);
            return false;
        }
        return true;
    }

    public int insertSanPhamMySQL(SanPham sanPham) {
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        if (conn != null) {
            try {
                ps = conn.prepareStatement("INSERT INTO SanPham(MaSanPham, TenSanPham, DonGia) values(?,?,?)");
                ps.setString(1, sanPham.getMaSanPham());
                ps.setString(2, sanPham.getTenSanPham());
                ps.setInt(3, sanPham.getDonGia());
                return ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return 0;
    }

    public int updateSanPhamMySQL(SanPham sanPham) {
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        if (conn != null) {
            try {
                String sql = "UPDATE SanPham SET TenSanPham = ?, DonGia = ? WHERE MaSanPham = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, sanPham.getTenSanPham());
                ps.setInt(2, sanPham.getDonGia());
                ps.setString(3, sanPham.getMaSanPham());
                return ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return 0;
    }

    public int deleteSanPhamMySQL(String maSanPham) {
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        if (conn != null) {
            try {
                String sql = "DELETE FROM SanPham WHERE MaSanPham = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, maSanPham);
                return ps.executeUpdate();
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return 0;
    }

    public ArrayList<SanPham> readSanPham() {
        ArrayList<SanPham> listSanPham = new ArrayList<>();
        Connection conn = mysql.getConnect();
        Statement stmt = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery("SELECT * FROM SanPham");
                while (rs.next()) {
                    SanPham sanPham = new SanPham();
                    sanPham.setMaSanPham(rs.getString("MaSanPham"));
                    sanPham.setTenSanPham(rs.getString("TenSanPham"));
                    sanPham.setDonGia(rs.getInt("DonGia"));
                    listSanPham.add(sanPham);
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return listSanPham;
    }

    public ArrayList<String> readListMaSanPham() {
        ArrayList<String> listMaSanPham = new ArrayList<>();
        Connection conn = mysql.getConnect();
        Statement stmt = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                stmt = conn.createStatement();
                rs = stmt.executeQuery("SELECT MaSanPham FROM SanPham");
                while (rs.next()) {
                    listMaSanPham.add(rs.getString("MaSanPham"));
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return listMaSanPham;
    }

    public SanPham layThongTinSanPham(String maSanPham) {
        SanPham sanPham = new SanPham();
        Connection conn = mysql.getConnect();
        PreparedStatement ps = null;
        ResultSet rs = null;
        if (conn != null) {
            try {
                String sql = "SELECT * FROM SanPham WHERE MaSanPham = ?";
                ps = conn.prepareStatement(sql);
                ps.setString(1, maSanPham);
                rs = ps.executeQuery();
                while (rs.next()) {
                    sanPham.setMaSanPham(rs.getString("MaSanPham"));
                    sanPham.setTenSanPham(rs.getString("TenSanPham"));
                    sanPham.setDonGia(rs.getInt("DonGia"));
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Lỗi kết nối cơ sở dữ liệu", "MySQL", 0);
            } finally {
                if (rs != null) {
                    try {
                        rs.close();
                    } catch (SQLException ex) {
                    }
                }
                if (ps != null) {
                    try {
                        ps.close();
                    } catch (SQLException ex) {
                    }
                }
                try {
                    conn.close();
                } catch (SQLException ex) {
                }
            }
        }
        return sanPham;
    }

}
