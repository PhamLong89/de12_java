/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java;

import com.mycompany.de12_java.View.BangCongView;
import com.mycompany.de12_java.View.SanPhamView;
import com.mycompany.de12_java.View.CongNhanView;
import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 *
 * @author long
 */
public class Main extends JFrame {

    private JTabbedPane tabbedPaneQuanLy;
    private CongNhanView congNhanView;
    private SanPhamView sanPhamView;
    private BangCongView bangCongView;

    public Main() {
        tabbedPaneQuanLy = new JTabbedPane();
        congNhanView = new CongNhanView();
        sanPhamView = new SanPhamView();
        bangCongView = new BangCongView();
        settingsJFrame();
        settingsJTabbedPane();
    }

    private void settingsJFrame() {
        this.setExtendedState(JFrame.MAXIMIZED_BOTH);
        this.setTitle("Quản lý công nhân làm việc");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLocationRelativeTo(null);
        this.add(tabbedPaneQuanLy);
    }

    private void settingsJTabbedPane() {
        tabbedPaneQuanLy.addTab("Quản lý công nhân", congNhanView);
        tabbedPaneQuanLy.addTab("Quản lý sản phẩm", sanPhamView);
        tabbedPaneQuanLy.addTab("Quản lý bảng tính công", bangCongView);
        tabbedPaneQuanLy.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
                int indexTab = tabbedPaneQuanLy.getModel().getSelectedIndex();
                switch (indexTab) {
                    case 0 ->
                        congNhanView.update();
                    case 1 ->
                        sanPhamView.update();
                    case 2 ->
                        bangCongView.update();
                    default -> {
                    }
                }       
            }
        });
    }

    public static void main(String[] args) {
        new Main().setVisible(true);
    }

}
