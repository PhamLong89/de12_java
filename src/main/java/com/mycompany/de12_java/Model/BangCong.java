/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java.Model;

import java.sql.Date;

/**
 *
 * @author long
 */
public class BangCong {

    private String maBangCong;
    private CongNhan congNhan;
    private Date ngayCong;
    private int tongTien;

    public String getMaBangCong() {
        return maBangCong;
    }

    public void setMaBangCong(String maBangCong) {
        this.maBangCong = maBangCong;
    }

    public CongNhan getCongNhan() {
        return congNhan;
    }

    public void setCongNhan(CongNhan congNhan) {
        this.congNhan = congNhan;
    }

    public Date getNgayCong() {
        return ngayCong;
    }

    public void setNgayCong(Date ngayCong) {
        this.ngayCong = ngayCong;
    }

    public int getTongTien() {
        return tongTien;
    }

    public void setTongTien(int tongTien) {
        this.tongTien = tongTien;
    }
}
