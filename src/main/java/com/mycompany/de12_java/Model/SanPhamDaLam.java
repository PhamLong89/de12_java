/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.de12_java.Model;

import com.mycompany.de12_java.Model.SanPham;

/**
 *
 * @author long
 */
public class SanPhamDaLam {

    private String maBangCong;
    private int maLamSanPham;
    private SanPham sanPham;
    private int soLuong;
    private int thanhTien;

    public String getMaBangCong() {
        return maBangCong;
    }

    public void setMaBangCong(String maBangCong) {
        this.maBangCong = maBangCong;
    }

    public int getMaLamSanPham() {
        return maLamSanPham;
    }

    public void setMaLamSanPham(int maLamSanPham) {
        this.maLamSanPham = maLamSanPham;
    }

    public SanPham getSanPham() {
        return sanPham;
    }

    public void setSanPham(SanPham sanPham) {
        this.sanPham = sanPham;
    }

    public int getSoLuong() {
        return soLuong;
    }

    public void setSoLuong(int soLuong) {
        this.soLuong = soLuong;
    }

    public int getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(int thanhTien) {
        this.thanhTien = thanhTien;
    }
}
