CREATE DATABASE De12_Java;
USE De12_Java;

CREATE TABLE CongNhan(
MaCongNhan varchar(10) not null primary key,
HoTen varchar(50),
DiaChi varchar(50),
CaSanXuat varchar(10),
SoDienThoai varchar(11));

CREATE TABLE SanPham(
MaSanPham varchar(10) not null primary key,
TenSanPham varchar(50), 
DonGia int);

CREATE TABLE BangCong(
MaBangCong varchar(10) not null primary key,
MaCongNhan varchar(10),
FOREIGN KEY (MaCongNhan) REFERENCES CongNhan(MaCongNhan),
NgayCong Date);

CREATE TABLE SanPhamDaLam(
MaBangCong varchar(10),
FOREIGN KEY (MaBangCong) REFERENCES BangCong(MaBangCong),
MaLamSanPham int not null primary key auto_increment,
MaSanPham varchar(10),
FOREIGN KEY (MaSanPham) REFERENCES SanPham(MaSanPham),
DonGia int,
SoLuong int);

DELIMITER &&
DROP FUNCTION IF EXISTS tinh_tong_tien;
CREATE FUNCTION tinh_tong_tien (ma_input varchar(10))
RETURNS INTEGER
BEGIN
	DECLARE tong_tien INT;
	SELECT SUM(sp.DonGia * sp.SoLuong)
	FROM SanPhamDaLam sp
	WHERE sp.MaBangCong = ma_input
	INTO tong_tien;
RETURN tong_tien;
END;
&&
DELIMITER ;

ALTER TABLE SanPhamDaLam MODIFY COLUMN MaLamSanPham auto_increment;
